import { loginKYC } from '@/services/auth.service';

const mutations = {
    login_setToken(state, token) {
        localStorage.setItem('token', token);
        state.status = "Authorization"
    },
    login_error(state, error) {
        state.error = error;
    },
    logout_success(state) {
        localStorage.removeItem('token')
        state.status = 'NoAuthorization';
    },
}

const actions = {
    login( {commit},){
        try{
            loginKYC()
        }catch(error){
            commit('login_error', error);
        }
    },
    setToken( { commit }, token){
        commit('login_setToken', token)
    },
    logout( { commit } ){
        commit('logout_success')
    },
}

const getters = {
    isAuthorisation: state => state.token,
    error: state => state.status,
}

const state = () => ({
    error: '',
    token: localStorage.getItem('token') || '',
    status: ''
})

export default {
    mutations,
    getters,
    actions,
    state,
}