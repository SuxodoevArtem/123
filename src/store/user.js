const mutations = {
    login_setToken(state, token) {
        localStorage.setItem('token', token);
        state.status = "Authorization"
    },
    login_error(state, error) {
        state.error = error;
    },
    logout_success(state) {
        localStorage.removeItem('token')
        state.status = 'NoAuthorization';
    },
}

const actions = {
    setToken( { commit }, token){
        console.log(token)
        commit('login_setToken', token)
    },
}

const getters = {
    isAuthorisation: state => state.token,
    error: state => state.status,
}

const state = () => ({
    user: {},
})

export default {
    mutations,
    getters,
    actions,
    state,
}