import { ReqScoring, GetReports } from '../services/scoring.service'

const mutations = {
    scoring_success(state, message) {
        state.status = message;
    },
    reports_success(state, data) {
        state.reports = data;
        console.log(data);
        console.log("reports")
    },
    /*scoring_error(state, message) {

    }*/
}

const actions = {
    async reqScoring( {commit}, ScoringMFO_V2){
        const { message } = await ReqScoring(ScoringMFO_V2);
        commit('scoring_success', message);
    },
    async getReports({commit}){
        console.log('*')
        const { data } = await GetReports();
        commit('reports_success', data);
    }
}

const getters = {
   Reports: state => state.reports,
   ReqStatus: state => state.status
   //SortOfScore: state => state.reports.map( {  } => )
}

const state = () => ({
    status: "",
    reports: [],
})

export default {
    mutations,
    getters,
    actions,
    state,
}