import axios from 'axios';

const instanceKYC = axios.create({
    baseURL: process.env.VUE_APP_KYC, 
});

instanceKYC.interceptors.request.use(function (config) {
    config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;
    return config;
}, function (error) {
    return Promise.reject(error);
})

const instanceNBKI = axios.create({
    baseURL: process.env.VUE_APP_SERVER,
});

export {
    instanceKYC,
    instanceNBKI,
}