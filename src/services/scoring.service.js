import { instanceNBKI } from '@/services/instances'

export const ReqScoring = async (data) => {  
    
    const response = await instanceNBKI.put('/scoring/reqScoring',data);

    console.log(response.message);
    return response.message;

}

export const GetReports = async () => {
    const response = await instanceNBKI.get('/scoring/getReports');
    console.log('*');
    console.log(response.message);
    return response.data;
}