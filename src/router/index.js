import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/index.js'

import Login from '@/views/Login.vue'
import Scoring from '@/views/Scoring.vue'
import MFO_V2 from '@/views/views_scoring/MFO_V2.vue'
import ScoringHistory from '@/views/views_scoring/ScoringHistory.vue'
import Error404 from '@/views/404.vue'

const reg_token = /access_token=.*&/;

Vue.use(VueRouter);
const routes = [
    {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: { layout: "EmptyLayout", requiresAuth: false }
    },
    {
        path: '/logout',
        name: 'Logout',
        component: Login,
        meta: { layout: "EmptyLayout", requiresAuth: false }
    },
    {
        path: '*',
        name: 'Logout',
        component: Error404,
        meta: { layout: "EmptyLayout", requiresAuth: false }
    },
    {
        path: '/scoring',
        name: 'Scoring',
        component: Scoring,
        children: [
            {
                path: '/scoring/mfo_v2',
                name: 'MFO_V2',
                component: MFO_V2,
                meta: { layout: "MainLayout", requiresAuth: true }
            },
            {
                path: '/scoring/scoring_history',
                name: 'ScoringHistory',
                component: ScoringHistory,
                meta: { layout: "MainLayout", requiresAuth: true }
            },
        ]
    },
]

const router = new VueRouter({
    mode: 'history',
    routes,
})

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isAuthorisation) {
          next()
          return
        }
        next('/login') 
      } else {
        next() 
    }

    if(to?.fullPath == '/logout'){
        store.dispatch('logout');
    }
    if(store.getters.isAuthorisation){
        next()
    }else{
        let token = null;

        try{
             token = to?.fullPath.match(reg_token)[0].split('=')[1].split('&')[0]
        }catch(e){
            console.log(e)
        }

        if(token){
            store.dispatch('setToken',token ); 
            window.location.replace(to.path);
            next()
        }
    }
})



export default router;